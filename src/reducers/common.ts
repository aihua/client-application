export interface IColumnGrid {
  name: string;
  title: string;
  dataType?: string;
}

export interface IStatus {
  isLoading: boolean;
  errMessage: string;
}
