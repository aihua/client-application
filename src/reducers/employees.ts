import { fromJS, List } from 'immutable';
import { EmployeeAction, EmployeesActions } from './../constants/employees';
import { IColumnGrid, IStatus } from './common';

export const initialState: List<IEmployeesState> = fromJS({
  columns: [
    { name: 'id', title: 'ID' },
    { name: 'name', title: 'Name' },
    { name: 'team', title: 'Team' },
    { name: 'district', title: 'District' },
    { name: 'age', title: 'Age', dataType: 'number' },
    { name: 'badEyesight', title: 'Has bad eyesight?', dataType: 'boolean' }
  ],
  employees: [],
  status: {
    errMessage: '',
    isLoading: false
  }
});

export default function (state: List<IEmployeesState> = initialState, action: EmployeeAction): List<IEmployeesState> {
  switch (action.type) {
    case EmployeesActions.TryToLoadEmployees:
      return state.setIn(['status', 'isLoading'], true);
    case EmployeesActions.EmployeesAreLoaded:
      state = state.setIn(['employees'], fromJS(action.employees));
      state = state.setIn(['status', 'isLoading'], false);
      state = state.setIn(['status', 'errMessage'], '');

      return state;
    case EmployeesActions.CannotLoadEmployees:
      state = state.setIn(['status', 'errMessage'], action.message);
      state = state.setIn(['status', 'isLoading'], false);

      return state;
    case EmployeesActions.CreateEmployee:
      return state.setIn(['employees', state.getIn(['employees']).size], fromJS(action.changes));
    case EmployeesActions.UpdateEmployee:
      return state.setIn(
        ['employees', action.index],
        fromJS({
          ...state.getIn(['employees', action.index]).toJS(),
          ...action.changes
        })
      );
    case EmployeesActions.RemoveEmployee:
      return state.deleteIn(['employees', action.index]);
    default:
      return state;
  }
}

export interface IEmployeesState {
  columns: IColumnGrid[];
  employees: IEmployee[];
  status: IStatus;
}

export interface IEmployee {
  id: number;
  name: string;
  team: string;
  district: string;
  age: number;
  badEyesight: boolean;
}