import { IEmployee } from './../reducers/employees';
import urlBuilder from './urlBuilder'

interface IEmplooyeesResponse {
  data: IEmployee[];
}

export default function (): Promise<IEmployee[]> {
  return fetch(urlBuilder('/employees/'))
    .then((res: Response): Promise<IEmplooyeesResponse> => res.json())
    .then((res: IEmplooyeesResponse) => res.data);
}
