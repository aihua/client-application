import urlBuilder from "./urlBuilder";

export default function (id: number, changes: object): Promise<any> {
  return fetch(urlBuilder(`/employees/${id}`), {
    body: JSON.stringify(changes),
    method: 'PUT'
  });
}
