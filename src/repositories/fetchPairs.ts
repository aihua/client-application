import { IPair } from "../reducers/pairs";
import urlBuilder from "./urlBuilder";

interface IPairResponse {
  data: IPair[];
}

export default function (): Promise<IPair[]> {
  return fetch(urlBuilder('/pairs/'))
    .then((res: Response): Promise<IPairResponse> => res.json())
    .then((res: IPairResponse) => res.data);
}
