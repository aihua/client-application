export interface IColors {
  primary: string;
  black: string;
  white: string;
}

export interface INavbarTheme {
  height: number;
}

export interface IContentTheme {
  width: number;
}

export default interface ISystemTheme {
  colors: IColors;
  navbar: INavbarTheme;
  content: IContentTheme;
}
