import { Action } from 'redux';
import { IEmployee } from './../reducers/employees';

export enum EmployeesActions {
  TryToLoadEmployees = 'TRY_TO_LOAD_EMPLOYEES',
  EmployeesAreLoaded = 'EMPLOYEES_ARE_LOADED',
  CannotLoadEmployees = 'CANNOT_LOAD_EMPLOYEES',

  TryToRemoveEmployee = 'TRY_TO_REMOVE_EMPLOYEE',
  RemoveEmployee = 'REMOVE_EMPLOYEE',

  CreateEmployee = 'CREATE_EMPLOYEE',
  UpdateEmployee = 'UPDATE_EMPLOYEE'
}

export interface ITryToLoadEmployeesEvent extends Action<EmployeesActions.TryToLoadEmployees> {}
export interface ILoadedEmployeesEvent extends Action<EmployeesActions.EmployeesAreLoaded> {
  employees: IEmployee[];
}
export interface IFailedEmployeesEvent extends Action<EmployeesActions.CannotLoadEmployees> {
  message: string;
}
export interface ICreateEmployeeEvent extends Action<EmployeesActions.CreateEmployee> {
  changes: object;
}
export interface IUpdateEmployeeEvent extends Action<EmployeesActions.UpdateEmployee> {
  changes: object;
  index: number;
}
export interface ITryToRemoveEmployeeEvent extends Action<EmployeesActions.TryToRemoveEmployee> {
  index: number;
}
export interface IRemoveEmployeeEvent extends Action<EmployeesActions.RemoveEmployee> {
  index: number;
}
export type EmployeeAction =
  | ITryToLoadEmployeesEvent
  | ILoadedEmployeesEvent
  | IFailedEmployeesEvent
  | ICreateEmployeeEvent
  | IUpdateEmployeeEvent
  | ITryToRemoveEmployeeEvent
  | IRemoveEmployeeEvent;
