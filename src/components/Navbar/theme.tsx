import styled from '../../theme';

export const Header = styled.header`
  width: 100%;
  height: ${props => props.theme.navbar.height}px;

  display: flex;
  justify-content: center;

  border-bottom: 2px solid ${props => props.theme.colors.primary};
`;

export const NavigationList = styled.ul`
  list-style-type: none;
  padding: 0 20px;

  display: flex;
  align-items: center;
  justify-content: flex-end;

  width: 100%;
  max-width: ${props => props.theme.content.width}px;
`;

export const NavigationItem = styled.li`
  height: ${props => props.theme.navbar.height}px;
  display: flex;
  align-items: center;
  justify-content: center;

  & > a {
    height: ${props => props.theme.navbar.height}px;
    line-height: ${props => props.theme.navbar.height}px;
    color: ${props => props.theme.colors.primary};

    font-size: 12px;
    text-transform: uppercase;

    padding: 0 20px;

    &:hover,
    &.active {
      background: ${props => props.theme.colors.primary};
      color: ${props => props.theme.colors.white};
      font-weight: 700;
    }
  }
`;
