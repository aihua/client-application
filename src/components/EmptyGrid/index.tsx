import * as React from 'react';
import { IStatus } from '../../reducers/common';
import { TableCell, TableRow } from './theme';

const EmptyGrid = ({ isLoading = false, errMessage = '' }: IStatus) => (): any => (
  <TableRow>
    <TableCell colSpan={6}>
      {isLoading === true && <p>Trwa ładowanie danych...</p>}
      {errMessage !== '' && <p>Nie udało się pobrać danych</p>}

      {!isLoading && '' === errMessage && <p>Brak danych do załadowania</p>}
    </TableCell>
  </TableRow>
);

export default EmptyGrid;
