import styled from '../../theme';

export const Main = styled.main`
  max-width: ${props => props.theme.content.width}px;

  margin: 0 auto;
  margin-top: 50px;
  padding: 0 20px;
`;
