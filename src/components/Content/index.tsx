import { Paper } from '@material-ui/core';
import * as React from 'react';
import { Main } from './theme';

interface IContent {
  children: any;
}

const Content = ({ children }: IContent) => (
  <Main>
    <Paper>{children}</Paper>
  </Main>
);

export default Content;
