import { fromJS } from 'immutable';
import { IColumnGrid } from './../../../reducers/common';
import { booleanColumns, numberColumns } from './../state';

it('should return an empty array when there is no boolean columns', () => {
  const columns: IColumnGrid[] = [
    { name: 'lorem-ipsum', title: 'Lorem ipsum' },
  ]

  const result = booleanColumns(fromJS(columns))
  expect(result).toHaveLength(0)
})

it('should return only columns with boolean values', () => {
  const columns: IColumnGrid[] = [
    { name: 'lorem-ipsum', title: 'Lorem ipsum' },
    { name: 'dolor-sit-amet', title: 'Dolor sit amet', dataType: 'boolean' }
  ]

  const result = booleanColumns(fromJS(columns))
  expect(result).toHaveLength(1)
})

it('should return an empty array when there is no number columns', () => {
  const columns: IColumnGrid[] = [
    { name: 'lorem-ipsum', title: 'Lorem ipsum' },
  ]

  const result = numberColumns(fromJS(columns))
  expect(result).toHaveLength(0)
})

it('should return only columns with number values', () => {
  const columns: IColumnGrid[] = [
    { name: 'lorem-ipsum', title: 'Lorem ipsum' },
    { name: 'dolor-sit-amet', title: 'Dolor sit amet', dataType: 'number' },
    { name: 'dolor-sit-amet', title: 'Dolor sit amet', dataType: 'boolean' }
  ]

  const result = numberColumns(fromJS(columns))
  expect(result).toHaveLength(1)
})