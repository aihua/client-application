import { EditingState } from '@devexpress/dx-react-grid';
import { Grid, Table, TableEditColumn, TableEditRow, TableHeaderRow } from '@devexpress/dx-react-grid-material-ui';
import { fromJS } from 'immutable';
import * as React from 'react';
import { connectWithLifecycle } from 'react-lifecycle-component';
import EmptyGrid from './../../components/EmptyGrid';
import BooleanTypeProvider from './components/BooleanTypeProvider';
import NumberTypeProvider from './components/NumberTypeProvider';
import { bindDispatchToProps, booleanColumns, IActions, IProps, mapStateToProps, numberColumns } from './state';

const EmployeesList = ({ employees = fromJS([]), columns = fromJS([]), status, commitChanges }: IProps & IActions) => (
  <Grid rows={employees.toJS()} columns={columns.toJS()}>
    <BooleanTypeProvider for={booleanColumns(columns)} />
    <NumberTypeProvider for={numberColumns(columns)} />
    <EditingState onCommitChanges={commitChanges} columnExtensions={[{ columnName: 'id', editingEnabled: false }]} />
    <Table noDataRowComponent={EmptyGrid(status)} />
    <TableHeaderRow />
    <TableEditRow />
    <TableEditColumn showAddCommand={true} showDeleteCommand={true} showEditCommand={true} />
  </Grid>
);

export default connectWithLifecycle(mapStateToProps, bindDispatchToProps)(EmployeesList);
