import { Chip } from '@material-ui/core';
import * as React from 'react';

interface IBooleanChip {
  value: boolean;
}

const BooleanFormatter = ({ value }: IBooleanChip) => <Chip label={value ? 'Yes' : 'No'} />;

export default BooleanFormatter;
