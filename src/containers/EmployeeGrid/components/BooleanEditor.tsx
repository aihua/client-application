import { Input, MenuItem, Select } from '@material-ui/core';
import * as React from 'react';

interface IBooleanEditor {
  value: boolean;
  onValueChange: any;
}

const onChange = (onValueChange: any) => (event: any) => onValueChange(event.target.value === 'Yes');

const BooleanEditor = ({ value, onValueChange }: IBooleanEditor) => (
  <Select input={<Input />} value={value ? 'Yes' : 'No'} onChange={onChange(onValueChange)} style={{ width: '100%' }}>
    <MenuItem value="Yes">Yes</MenuItem>
    <MenuItem value="No">No</MenuItem>
  </Select>
);

export default BooleanEditor;
