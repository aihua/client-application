import { DataTypeProvider } from '@devexpress/dx-react-grid';
import * as React from 'react';
import NumberEditor from './NumberEditor';
import NumberFormatter from './NumberFormatter';

const NumberTypeProvider = (props: any) => (
  <DataTypeProvider formatterComponent={NumberFormatter} editorComponent={NumberEditor} {...props} />
);

export default NumberTypeProvider;
