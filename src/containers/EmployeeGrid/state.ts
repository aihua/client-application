import { ChangeSet } from '@devexpress/dx-react-grid';
import { List } from 'immutable';
import { Dispatch } from 'redux';
import { IStore } from '../../reducers';
import { IEmployee } from '../../reducers/employees';
import { onGridCommitChanges, tryToLoadEmployees } from './../../actions/employees';
import { IColumnGrid, IStatus } from './../../reducers/common';

export interface IProps {
  employees: List<IEmployee[]>;
  columns: List<IColumnGrid[]>;
  status: IStatus;
}
export interface IActions extends React.ComponentLifecycle<IProps, {}> {
  commitChanges: (changes: ChangeSet) => void;
}

export function mapStateToProps(state: List<IStore>): IProps {
  return {
    columns: state.getIn(['employees', 'columns']),
    employees: state.getIn(['employees', 'employees']),
    status: state.getIn(['employees', 'status'])
  };
}

export function bindDispatchToProps(dispatch: Dispatch): IActions {
  return {
    commitChanges: (changes: ChangeSet) => onGridCommitChanges(dispatch)(changes),
    componentWillMount: () => dispatch(tryToLoadEmployees())
  };
}

export function booleanColumns(columns: List<IColumnGrid[]>): string[] {
  return columns
    .toJS()
    .filter((column: IColumnGrid): boolean => undefined !== column.dataType && column.dataType === 'boolean')
    .map((column: IColumnGrid): string => column.name);
}

export function numberColumns(columns: List<IColumnGrid[]>): string[] {
  return columns
    .toJS()
    .filter((column: IColumnGrid): boolean => undefined !== column.dataType && column.dataType === 'number')
    .map((column: IColumnGrid): string => column.name);
}
