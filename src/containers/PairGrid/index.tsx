import { Grid, Table, TableHeaderRow } from '@devexpress/dx-react-grid-material-ui';
import { fromJS } from 'immutable';
import * as React from 'react';
import { connectWithLifecycle } from 'react-lifecycle-component';
import EmptyGrid from './../../components/EmptyGrid';
import { bindDispatchToProps, IActions, IProps, mapStateToProps } from './state';

const EmployeesList = ({ employees = fromJS([]), columns = fromJS([]), status }: IProps & IActions) => (
  <Grid rows={employees.toJS()} columns={columns.toJS()}>
    <Table noDataRowComponent={EmptyGrid(status)} />
    <TableHeaderRow />
  </Grid>
);

export default connectWithLifecycle(mapStateToProps, bindDispatchToProps)(EmployeesList);
