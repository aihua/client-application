import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import EmployeesList from './screens/EmployeesList';
import MatchedEmployees from './screens/MatchedEmployees'

const Routing = () => (
  <Switch>
    <Route path={'/'} exact={true} component={EmployeesList} />
    <Route path={'/pairs'} exact={true} component={MatchedEmployees} />
  </Switch>
);

export default Routing;
