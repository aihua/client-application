import * as React from 'react';
import EmployeeGrid from './../../containers/EmployeeGrid';

const EmployeesList = () => <EmployeeGrid />;

export default EmployeesList;
